import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HotToastService } from '@ngneat/hot-toast';
import { Route, Router } from '@angular/router';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { AnimationOptions } from 'ngx-lottie';


interface FormLogin {
  email: string | null,
  password: string | null
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class LoginComponent {
  
    email: string = '';
    password: string = '';
    isPass: boolean = true;

  formLogin!: FormGroup

  lottieConfig: AnimationOptions = {
    path: 'assets/lottie/recycle1.json'
  }
  constructor(
    private authService: AuthService, 
    private toastService: HotToastService, 
    private router: Router, ) { 
      this.formLogin= new FormGroup({
        email: new FormControl<string | null> ("", {
          validators: [Validators.required, Validators.email]
        }),
        password: new FormControl<string | null> ("", {
          validators: [Validators.required, Validators.minLength(8)]
        }),
      })
  }

  get errorControl(){
    return this.formLogin.controls;
  }

  doLogin(){
    const payload = {
      email: this.formLogin.value.email,
      password: this.formLogin.value.password
    }
    this.authService.login(payload).subscribe({
      next: (val) => {
        this.toastService.success()
        localStorage.setItem('token', val.token)
        this.authService.token = val.token
        this.router.navigate(['/home'])
        console.log(val);
      },
      error: (err) => {
        this.toastService.error("This didn't work.")
        //console.log(err);
      } 
    })
  }

  doChangeType(){
    this.isPass = !this.isPass
  }
}
