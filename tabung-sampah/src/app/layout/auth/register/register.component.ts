import { ChangeDetectionStrategy, Component} from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HotToastService } from '@ngneat/hot-toast';
import { Route, Router } from '@angular/router';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';

interface formRegister {
  name: string | null,
  email: string | null,
  phone: string | null,
  password: string | null,
  cpassword: string | null
}
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class RegisterComponent {

  name: string = '';
  email: string = '';
  phone: string = '';
  password: string = '';
  alamat: string = '';

  isPass: boolean = true;
  formRegister!: FormGroup

  constructor(
    private authService: AuthService, 
    private toastService: HotToastService, 
    private router: Router) { 
      this.formRegister= new FormGroup ({
        name: new FormControl<string | null> ("", {
          validators: [Validators.required]
        }),
        email: new FormControl<string | null> ("", {
          validators: [Validators.required, Validators.email]
        }),
        phone: new FormControl<string | null> ("", {
          validators: [Validators.required, Validators.minLength(10)]
        }),
        password: new FormControl<string | null> ("", {
          validators: [Validators.required, Validators.minLength(8)]
        }),
        alamat: new FormControl<string | null> ("", {
          validators: [Validators.required]
        })
      })
    }

  get errorControl(){
    return this.formRegister.controls;
  }

  doRegister(){
    const payload = {
      name: this.formRegister.value.name,
      email: this.formRegister.value.email,
      phone: this.formRegister.value.phone,
      password: this.formRegister.value.password,
      alamat: this.formRegister.value.alamat
    }
    this.authService.register(payload).subscribe({
      next: (val) => {
        this.toastService.success(val.message)
        this.router.navigate(['/login'])
      },
      error: (err) => {
        this.toastService.error(err.error.message)
      } 
    })
  }
  
  doChangeType(){
    this.isPass = !this.isPass
  }
}
