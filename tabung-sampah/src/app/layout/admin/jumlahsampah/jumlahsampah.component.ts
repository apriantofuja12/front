import { ChangeDetectionStrategy, Component, ElementRef, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { TransaksiService } from 'src/app/services/transaksi.service';
import { AmountOfGarbageData } from  'src/app/model/transaksi.model';

@Component({
  selector: 'app-jumlahsampah',
  templateUrl: './jumlahsampah.component.html',
  styleUrls: ['./jumlahsampah.component.scss'],
})
export class JumlahsampahComponent  {

  @ViewChild('barCanvas') barCanvas: ElementRef | undefined;
  barChart: any;

  amountOfGarbages:AmountOfGarbageData[] = [];

  constructor(private transaksiService: TransaksiService) { }

  ngAfterViewInit(): void {
     this.barChartMethod();
     this.getAmountOfGarbage();
  }

  getAmountOfGarbage() {
    this.transaksiService.amountOfGarbage()
      .subscribe(data => {
        console.log(data)
        this.amountOfGarbages = data;
      }, error => { throw new Error('Cannot get Amount of Garbage Data.'); });
  }
 
  barChartMethod() {
    this.barChart = new Chart(this.barCanvas?.nativeElement, {
      type: 'bar',
      data: {
        labels: ['Plastik', 'Kertas', 'Botol', 'Kain', 'Lainnya', 'Total'],
        datasets: [
          {
            label: 'Total Sampah',
            data: [10, 10, 10, 10, 10, 50],
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)',
            ],
            borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)',
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }

}
