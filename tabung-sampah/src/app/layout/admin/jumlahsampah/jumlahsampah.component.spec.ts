import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JumlahsampahComponent } from './jumlahsampah.component';

describe('JumlahsampahComponent', () => {
  let component: JumlahsampahComponent;
  let fixture: ComponentFixture<JumlahsampahComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JumlahsampahComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JumlahsampahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
