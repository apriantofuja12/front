import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApakekComponent } from './apakek.component';

describe('ApakekComponent', () => {
  let component: ApakekComponent;
  let fixture: ComponentFixture<ApakekComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApakekComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApakekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
