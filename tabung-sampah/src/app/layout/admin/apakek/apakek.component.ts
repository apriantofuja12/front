import { DecimalPipe } from '@angular/common';
import { AfterViewInit, Component, ElementRef, PipeTransform, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, startWith, map } from 'rxjs';
import Chart from 'chart.js/auto';

interface Country {
  name: string;
  flag: string;
  area: number;
  population: number;
}

const COUNTRIES: Country[] = [
  {
    name: 'Russia',
    flag: 'f/f3/Flag_of_Russia.svg',
    area: 17075200,
    population: 146989754
  },
  {
    name: 'Canada',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: 9976140,
    population: 36624199
  },
  {
    name: 'United States',
    flag: 'a/a4/Flag_of_the_United_States.svg',
    area: 9629091,
    population: 324459463
  },
  {
    name: 'China',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: 9596960,
    population: 1409517397
  }
];

function search(text: string, pipe: PipeTransform): Country[] {
  return COUNTRIES.filter(country => {
    const term = text.toLowerCase();
    return country.name.toLowerCase().includes(term)
        || pipe.transform(country.area).includes(term)
        || pipe.transform(country.population).includes(term);
  });
}

const data = {
  labels: [
    'Red',
    'Green',
    'Yellow',
    'Grey',
    'Blue'
  ],
  datasets: [{
    label: 'My First Dataset',
    data: [11, 16, 7, 3, 14],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(75, 192, 192)',
      'rgb(255, 205, 86)',
      'rgb(201, 203, 207)',
      'rgb(54, 162, 235)'
    ]
  }]
};

const config = {
  type: 'polarArea',
  data: data,
  options: {}
};


@Component({
  selector: 'app-apakek',
  templateUrl: './apakek.component.html',
  styleUrls: ['./apakek.component.scss'],
  providers: [DecimalPipe]
})
export class ApakekComponent implements AfterViewInit {

  countries$: Observable<Country[]>;
  filter = new FormControl('', {nonNullable: true});

  polarChart: any;
  canvas: any;
  ctx: any;
  @ViewChild('bar') polarCanvas!: { nativeElement: any };

  // @ViewChild('barCanvas') barCanvas: ElementRef | undefined;
  // barChart: any;
 

  constructor(pipe: DecimalPipe) {
    this.countries$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => search(text, pipe))
    );
  }

  ngAfterViewInit(): void {
   // this.pieChartBrowser();
    //this.barChartMethod
    this.polarChartBrowser();
  }

  // pieChartBrowser(): void {
  //   this.canvas = this.pieCanvas.nativeElement;
  //   this.ctx = this.canvas.getContext('2d');

  //   this.pieChart = new Chart(this.ctx, {
  //     type: 'pie',
  //     data: {
  //       labels: ['Apple', 'Google', 'Facebook', 'Infosys', 'Hp', 'Accenture'],
  //       datasets: [
  //         {
  //           backgroundColor: [
  //             '#2ecc71',
  //             '#3498db',
  //             '#95a5a6',
  //             '#9b59b6',
  //             '#f1c40f',
  //             '#e74c3c',
  //           ],
  //           data: [12, 19, 3, 17, 28, 24],
  //         },
  //       ],
  //     },
  //   });
  // }
  polarChartBrowser(): void {
    this.canvas = this.polarCanvas.nativeElement;
    this.ctx = this.canvas.getContext('2d');

    this.polarChart = new Chart(this.ctx, {
      type: 'polarArea',
      data: data
    });
  }




  // barChartMethod() {
  //   this.barChart = new Chart(this.barCanvas?.nativeElement, {
  //     type: 'bar',
  //     data: {
  //       labels: ['BJP', 'INC', 'AAP', 'CPI', 'CPI-M', 'NCP'],
  //       datasets: [
  //         {
  //           label: '# of Votes',
  //           data: [200, 50, 30, 15, 20, 34],
  //           backgroundColor: [
  //             'rgba(255, 99, 132, 0.2)',
  //             'rgba(54, 162, 235, 0.2)',
  //             'rgba(255, 206, 86, 0.2)',
  //             'rgba(75, 192, 192, 0.2)',
  //             'rgba(153, 102, 255, 0.2)',
  //             'rgba(255, 159, 64, 0.2)',
  //           ],
  //           borderColor: [
  //             'rgba(255,99,132,1)',
  //             'rgba(54, 162, 235, 1)',
  //             'rgba(255, 206, 86, 1)',
  //             'rgba(75, 192, 192, 1)',
  //             'rgba(153, 102, 255, 1)',
  //             'rgba(255, 159, 64, 1)',
  //           ],
  //           borderWidth: 1,
  //         },
  //       ],
  //     },
  //     options: {
  //       scales: {
  //         y: {
  //           beginAtZero: true,
  //         },
  //       },
  //     },
  //   });
  // }

}
