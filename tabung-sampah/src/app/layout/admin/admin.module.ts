import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { PlaygroundComponent } from './playground/playground.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module copy';
import { JumlahsampahComponent } from './jumlahsampah/jumlahsampah.component';
import { LottieModule } from 'ngx-lottie';
import { ApakekComponent } from './apakek/apakek.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HistoryComponent } from './history/history.component';
import { PickandpackComponent } from './pickandpack/pickandpack.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';



@NgModule({
  declarations: [
    AdminComponent, 
    PlaygroundComponent,
    JumlahsampahComponent,
    ApakekComponent,
    HistoryComponent,
    PickandpackComponent,
    HomeComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    LottieModule,
    TranslateModule, 
    NgbModule
  ]
})
export class AdminModule { }
