import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ApakekComponent } from './apakek/apakek.component';
import { HistoryComponent } from './history/history.component';
import { HomeComponent } from './home/home.component';
import { JumlahsampahComponent } from './jumlahsampah/jumlahsampah.component';
import { PickandpackComponent } from './pickandpack/pickandpack.component';
import { PlaygroundComponent } from './playground/playground.component';
import { ProfileComponent } from './profile/profile.component';



const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
        {
          path: 'playground',
          component: PlaygroundComponent
        },
        {
            path: 'history',
            component: HistoryComponent
        },
        {
            path: 'jumlahsampah',
            component: JumlahsampahComponent
        },
        {
          path: 'pickandpack',
          component: PickandpackComponent
        },
        {
          path: 'home',
          component: HomeComponent
        },
        {
          path: 'profile',
          component: ProfileComponent
        }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }