import { DecimalPipe } from '@angular/common';
import { AfterViewInit, Component, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, startWith, map } from 'rxjs';
import { TransaksiService } from 'src/app/services/transaksi.service';
import { HistoryData } from  'src/app/model/transaksi.model';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  providers: [DecimalPipe]
})
export class HistoryComponent implements AfterViewInit {

  //countries$: Observable<Country[]> | undefined;
  filter = new FormControl('', {nonNullable: true});
  
  historys:HistoryData[] = [];

  constructor(pipe: DecimalPipe, private transaksiService: TransaksiService) { }

  ngAfterViewInit(): void {
    this.getHistory()
  }

  getHistory() {
    this.transaksiService.history('')
      .subscribe(data => {
        console.log(data)
        this.historys = data;
      }, error => { throw new Error('Cannot get History Data.'); });
  }

}
