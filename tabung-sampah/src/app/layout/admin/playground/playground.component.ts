import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DataAccesService } from 'src/app/services/data-acces.service';
import { Person } from './playground.utils';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})

export class PlaygroundComponent implements OnInit {
  name: string = ' ';
  age: number = 0;
  show: boolean = true;
  edit: boolean = false;

  person: Person[] = []

  constructor(private dataAccessService : DataAccesService) { }
 
  ngOnInit(): void {
    this.person = this.dataAccessService.getPerson()
  }

  doAddPerson() {
    if(this.name === '') return alert('Name must be no null')
    if(this.age < 0) return alert('Age must be no minus')
    const payload = {
      name: this.name,
      age: this.age
    }
    this.dataAccessService.addPerson(payload);
    this.formReset();
  }

  doDelete(ev: Person) {
    this.person = this.dataAccessService.deletePerson(ev)
  }

  showbutton(){
    if (this.show == false){
      this.show = true
    } else {
      this.show = false
    }
  }

  formReset(){
    this.name = '',
    this.age = 0
  }

  doEdit(ev: Person) {
    this.edit = true
    this.name = ev.name
    this.age = ev.age
  }

  doEditPerson(){
    this.edit = false
    this.formReset();
  }
}
