import { AfterViewInit, ChangeDetectionStrategy, Component } from '@angular/core';
import { TransaksiData } from 'src/app/model/transaksi.model';
import { TransaksiService } from 'src/app/services/transaksi.service';

@Component({
  selector: 'app-pickandpack',
  templateUrl: './pickandpack.component.html',
  styleUrls: ['./pickandpack.component.scss']
})
export class PickandpackComponent implements AfterViewInit {
  name: string = '';
  sampahId: number = 0;
  jumlah: number = 0;

  constructor(private transaksiService: TransaksiService) { }

  ngAfterViewInit(): void {
  }

  postTransaksi() {
    const payload = {
      name: this.name,
      sampahId: this.sampahId,
      jumlah: this.jumlah
    }
    this.transaksiService.transaksi(payload).subscribe(data => {
        console.log(data)
      }, error => { throw new Error('Error'); });
  }
}
