import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickandpackComponent } from './pickandpack.component';

describe('PickandpackComponent', () => {
  let component: PickandpackComponent;
  let fixture: ComponentFixture<PickandpackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickandpackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PickandpackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
