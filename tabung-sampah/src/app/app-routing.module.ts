import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [ 
  {
    path: '',
    loadChildren: () => import('./layout/auth/auth.module').then((m) => m.AuthModule)
  },
  {
    path: '',
    loadChildren: () => import('./layout/admin/admin.module').then((m) => m.AdminModule),
    canLoad: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/playground',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
