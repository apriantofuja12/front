import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { TransaksiData } from '../model/transaksi.model';

@Injectable({
  providedIn: 'root'
})
export class TransaksiService {

  private readonly baseApi = 'http://localhost:8181/'

  constructor(private httpClient: HttpClient) {  }

  amountOfGarbage(): Observable<any> {
    const token = localStorage.getItem('token');
        let headersDict = {
          Authorization: 'Bearer ' + token
        };
        let requestOptions = {
          headers: new HttpHeaders(headersDict),
        };
    return new Observable((observer: Observer<any>) => {
      this.httpClient.get(`${this.baseApi}transaksi/amountofgarbage`, requestOptions).subscribe(
        (response: any) => {
          observer.next(response);
        }, error => {
          observer.error(error.message);
        });
    });
  }

  history(name:string): Observable<any> {
    const token = localStorage.getItem('token');
        let headersDict = {
          Authorization: 'Bearer ' + token
        };
        let requestOptions = {
          headers: new HttpHeaders(headersDict),
        };
    return new Observable((observer: Observer<any>) => {
      this.httpClient.get(`${this.baseApi}transaksi/history?name=${name}`, requestOptions).subscribe(
        (response: any) => {
          observer.next(response);
        }, error => {
          observer.error(error.message);
        });
    });
  }

  transaksi(requestBody: TransaksiData): Observable<any> {
    const token = localStorage.getItem('token');
    let headersDict = {
      Authorization: 'Bearer ' + token
    };
    let requestOptions = {
      headers: new HttpHeaders(headersDict),
    };
    return new Observable((observer: Observer<any>) => {
      this.httpClient.post(`${this.baseApi}transaksi`, requestBody, requestOptions).subscribe(
        (response: any) => {
          observer.next(response);
        }, error => {
          observer.error(error.message);
        });
    });
  } 

}