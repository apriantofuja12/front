import { Injectable } from '@angular/core';
import { Person } from '../layout/admin/playground/playground.utils';

@Injectable({
  providedIn: 'root'
})
export class DataAccesService {

  private person: Person[] = [ ]
  constructor() { }

  getPerson(): Person[] {
    return this.person
  }
  
  addPerson(payload: Person) {
    this.person.push(payload)
  }

  deletePerson(payload: Person){
    this.person = this.person.filter(data => data !== payload)
    return this.person
  }
}
