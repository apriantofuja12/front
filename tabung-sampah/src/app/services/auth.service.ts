import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';

interface requestLogin {
  email: string,
  password: string
}
interface register {
  name: string,
  email: string,
  phone: string,
  password: string,
  alamat: string
}

interface responseBase {
  message: string
}

interface responseLogin {
  token: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly baseApi = 'http://localhost:8181/'

  private _token: string = ''
  public isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

  constructor(private httpClient: HttpClient) {
    this.loadToken()
  }

  login(payload: requestLogin): Observable<responseLogin> {
    return this.httpClient.post<responseLogin>(`${this.baseApi}login`, payload).pipe(
      tap( () => {
        this.isAuthenticated.next(true)
      })
    )
  }

  register(payload: register): Observable<responseBase>{
    return this.httpClient.post<responseBase>(`${this.baseApi}register`, payload)
  }

  loadToken(){
    const token = localStorage.getItem('token');
    if (token) {
      this.token = token
      this.isAuthenticated.next(true)
    } else {
      this.isAuthenticated.next(false)
    }
  }

  clearToken(){
    localStorage.clear()
    this.isAuthenticated.next(false)
  }

  set token(ev:string){
    this._token = ev;
  }

  get token(){
    return this._token;
  }
}
