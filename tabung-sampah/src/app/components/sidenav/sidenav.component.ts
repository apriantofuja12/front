import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent {

  constructor(private translate: TranslateService, private authService: AuthService) { }

  doTranslate(languange: string){
    this.translate.setDefaultLang(languange)
    this.translate.use(languange)
  }

  doLogout(){
    this.authService.clearToken()
  }

}
