import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { Router, RouterModule } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { TranslateModule } from '@ngx-translate/core';




@NgModule({
  declarations: [
    HeaderComponent, 
    SidenavComponent, 
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
  ],
  exports: [
    HeaderComponent, 
    SidenavComponent
  ]
})
export class ComponentsModule { }
