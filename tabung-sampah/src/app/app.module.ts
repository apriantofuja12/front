import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HotToastModule } from '@ngneat/hot-toast';
import { ComponentsModule } from './components/components.module';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web'
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpinterceptorService } from './services/httpinterceptor.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


export const playerFactory = () => {
  return player;
}
export const httpLoaderFactory = (http: HttpClient) => {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HotToastModule.forRoot(),
    ComponentsModule,
    NgbModule,
    LottieModule.forRoot({player: playerFactory}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: HttpinterceptorService, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
