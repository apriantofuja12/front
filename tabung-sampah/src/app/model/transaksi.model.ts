export interface AmountOfGarbageData {
    bulan: string,
    sampahPlastik: number,
    sampahKertas: number,
    sampahBotol: number,
    sampahKain: number,
    sampahLainnya: number,
    total: number
  }
  
  export interface HistoryData {
    name: string,
    address: string,
    total: number,
    totalHarga: number
  }

  export interface TransaksiData {
    name: string,
    sampahId: number,
    jumlah: number
  }